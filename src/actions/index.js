import * as types from '../constants/ActionTypes'
import axios from "axios";

export const fetchProducts = ($page) => async dispatch => {
  dispatch(setCurrentPage($page))
  const REACT_APP_BASE_URL = process.env.REACT_APP_BASE_URL;
  const baseURL = REACT_APP_BASE_URL + `/api/v1/product/index?page=${$page}`;
  const data  = await axios.get(baseURL);
  const response = data.data;
  dispatch({
    type: 'FETCH_PRODUCTS',
    payload: {
     products: response.data,
     last_page : response.last_page,
     filters :  response.filters,
     images : response.images,
     products_total_number:response.total,
     loading : false
    }
  })
}


export const filterProducts = ($filter_array) =>async (dispatch) => {
  const REACT_APP_BASE_URL = process.env.REACT_APP_BASE_URL;
  const baseURL = REACT_APP_BASE_URL + `/api/v1/filter/filter-products`;
  const data  = await axios.post(baseURL,$filter_array);
  const response = data.data;
  dispatch({
    type: types.FILTER_PRODUCTS,
    payload: {
      products: response.data,
      last_page : response.last_page,
      products_total_number:response.total,
      loading : false
    }
  })
  hideLoader()
}

export const showLoader = () => (dispatch) => {
 
  dispatch({
    type: types.SHOW_LOADER,
  });
};

export const hideLoader = () => (dispatch) => {
  dispatch({
    type: types.HIDE_LOADER,
  });
};

export const setCurrentPage = (page) => (dispatch) => {
  dispatch({
    type: types.SET_CURRENT_PAGE,
    payload: {
      current_page: page,
    }
  });
};

