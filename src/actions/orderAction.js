import * as types from '../constants/ActionTypes'
import axios from "axios";

export const resetOrder= () => (dispatch,getState) => {
    localStorage.removeItem("cart");
    dispatch({
        type: types.RESET_ORDER,
        payload: {
            order : [],
            cart : []
        }
    })
}