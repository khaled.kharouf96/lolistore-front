import * as types from '../constants/ActionTypes'
import axios from "axios";
import { json } from 'react-router-dom';

export const addToCart = (product) => (dispatch,getState) => {
    const cart =  getState().cart.cart.slice();
    let alreadyExist = false;
    cart.forEach(element => {
        if(element.id === product.id){
            alreadyExist = true;
            element.count++;
        }
    });
    if(!alreadyExist){
        cart.push({...product,count:1});
    }
    dispatch({
        type: types.ADD_TO_CART,
        payload: {
            cart : cart,
        }
    })
    localStorage.setItem("cart",JSON.stringify(cart));
}

export const removeFromCart= (product) => (dispatch,getState) => {
    const cart =  getState().cart.cart.slice().filter(x => x.id !== product.id);
    dispatch({
        type: types.REMOVE_FROM_CART,
        payload: {
            cart : cart,
        }
    })
    localStorage.setItem("cart",JSON.stringify(cart));
}

export const decrementQuantity= (product) => (dispatch,getState) => {
    const cart =  getState().cart.cart.slice();
    cart.forEach(element => {
        if(element.id === product.id){
            element.count--;
        }
    });
    dispatch({
        type: types.DECREMENT_QUANITY_PRODUCT,
        payload: {
            cart : cart,
        }
    })
    localStorage.setItem("cart",JSON.stringify(cart));
}

export const setShippingCost = (cost) => (dispatch,getState) => {
    dispatch({
        type: types.SET_SHIPPING_COST,
        payload: {
            shipping_cost : cost,
        }
    })
}

export const resetCart = () => (dispatch) => {
    dispatch({
        type: types.SET_SHIPPING_COST,
    })
}

