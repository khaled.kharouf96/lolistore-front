import * as types from '../constants/ActionTypes' 

export const cartReducer = (state={cart:JSON.parse(localStorage.getItem('cart') || "[]")},action) => {
    switch(action.type){
        case types.ADD_TO_CART:
            return {
                ...state,
                cart: action.payload.cart,
            };
        case types.REMOVE_FROM_CART:
            return {
                ...state,
                cart: action.payload.cart,
            };
        case types.DECREMENT_QUANITY_PRODUCT:
            return {
                ...state,
                cart: action.payload.cart,
            };
        case types.SET_SHIPPING_COST:
            return {
                ...state,
                shipping_cost : action.payload.shipping_cost,

            };
        case types.RESET_CART:
            return {
                cart: [],
                shipping_cost : [],

            };
        default:
            return state;
    }
}