import * as types from '../constants/ActionTypes' 

export const orderReducer = (state={cart:JSON.parse(localStorage.getItem('cart') || "[]")},action) => {
    switch(action.type){
        case types.RESET_ORDER:
            return {
                cart: [],
            };
        default:
            return state;
    }
}