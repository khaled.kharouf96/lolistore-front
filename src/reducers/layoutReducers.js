import * as types from '../constants/ActionTypes' 

export const layoutReducer = (state={},action) => {
    switch(action.type){
        case types.BLOUR_LAYOUT:
            return {
                ...state,
                blour_status : action.payload.status
            };
        default:
            return state;
    }
}