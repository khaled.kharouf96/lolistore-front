import * as types from '../constants/ActionTypes' 

export const productsReducer = (state={},action) => {
    switch(action.type){
        case 'FETCH_PRODUCTS':
            return {
                ...state,
                items:action.payload.products,
                filters:action.payload.filters,
                images:action.payload.images,
                last_page:action.payload.last_page,
                products_total_number:action.payload.products_total_number,
            }
        case types.FILTER_PRODUCTS :
            return {...state,
                    items:action.payload.products,
                    last_page:action.payload.last_page,
                    products_total_number:action.payload.products_total_number,
                }
        case types.HIDE_LOADER:
            return {
                ...state,
                loading: false,
            };
        case types.SHOW_LOADER:
            return {
                ...state,
                loading: true,
            };
        case types.SET_CURRENT_PAGE:
            return {
                ...state,
                current_page: action.payload.current_page,
            };
        default:
            return state;
            
    }
}