import React from 'react';
import ReactDOM from 'react-dom/client'
import './index.css';
import App from './App.js';
import { ChakraProvider } from '@chakra-ui/react';
import reportWebVitals from './reportWebVitals.js';
import './i18n/config';
import { Provider } from 'react-redux';
import {productsReducer} from './reducers/productReducers';
import { cartReducer } from './reducers/cartReducers';
import { orderReducer } from './reducers/orderReducers';
import { layoutReducer } from './reducers/layoutReducers';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger'
import Notifications, {notify} from 'react-notify-toast';
import { createStore, applyMiddleware, combineReducers,compose } from 'redux';
import 'tw-elements';
import 'flowbite';

const initialState = {
  loading : false
};
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; 

const middleware = [ thunk ];
const store = createStore(combineReducers({
  products : productsReducer,
  cart : cartReducer,
  order : orderReducer,
  layout: layoutReducer
  }),initialState, composeEnhancer(applyMiddleware(...middleware))
)

// store.dispatch(fetchProducts())
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <ChakraProvider>
  <React.StrictMode>
    <Provider store={store}>
    <Notifications />
    <App />
    </Provider>
  </React.StrictMode>
  </ChakraProvider>

);


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
