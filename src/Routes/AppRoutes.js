import { Route, Routes } from 'react-router-dom'
import Checkout  from '../pages/Checkout/Checkout';
import Home from '../pages/Home/Home';
const AppRoutes = () =>{
    <Routes>
        <Route
               path="/home"
               element={
                   <Home />
                }
        >
            <Route
               path="/checkout"
               element={
                   <Checkout />
                }
            />
        </Route>
       
    </Routes>
}

export default AppRoutes;