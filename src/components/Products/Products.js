import Product from "./Product";
import React, { useEffect, useState } from 'react';
import axios from "axios";
import Pagination from '../Pagination/Pagination';
import NoContent from "../Cards/No-Content/NoContent";
import Loading from "../Cards/Loading/Loading";
import { connect } from "react-redux";
import { fetchProducts, showLoader} from '../../actions/index'
import { useTranslation } from "react-i18next";


function Products({showAsRow,productsState,last_page,loading}) {
  const [products,setProducts] = useState([])
  const { t } = useTranslation();
  const [showProducts,setShowProducts] = useState(
    false
   )


  const paginateProducts = ($page) => { 
      // fetchProducts($page)

  }

  const filterProducts = (smell=1)=>{
    const products_filtered = products
                  .filter(product => 
                   product.smell != null && product.smell.id == smell
                ) 
      console.log(products_filtered)
      setProducts(products_filtered)
  }

  return (
    <div id='product'>
      {
        !productsState || loading 
        ?
        <Loading></Loading> 
        :
        <>
        {
          Object. keys(productsState).length == 0
          ?
            <NoContent text={t('no products')}></NoContent>
          :
          <>
          <section className="blur-after-click-filter grid-row-style" >
          { (showAsRow)
            ?
            <div className="grid grid-cols-1 gap-7 md:grid-cols-1">
              {
                productsState.map((product,key) =>
                  <Product 
                    type={'row'}
                    id={product.children.length > 0 ?product.childrenItems['id'] :product.id }
                    name={product.name}
                    key = {key}
                    description = {product.description}
                    price = {product.children.length > 0 ?product.childrenItems['price'] :product.price }
                    image = {product.children.length > 0 ?product.childrenItems['image'] :product.image }
                    smell = {product.smell}
                    size = {product.size}
                    children ={product.children}
                    product = {product.children.length > 0 ? product.children.slice().filter(a => a.id ==product.childrenItems.id ) :product}
                  >
                  </Product>
                )
              }
            </div>
            :
            <div className="grid grid-cols-2 gap-4 md:grid-cols-4  ">
                {
                  productsState.map((product,key) =>
                    <Product 
                      type={'col'}
                      id={product.children.length > 0 ?product.childrenItems.id :product.id }
                      name={product.name}
                      key = {key}
                      description = {product.description}
                      price = {product.children.length > 0 ?product.childrenItems.price :product.price }
                      image = {product.children.length > 0 ?product.childrenItems.image :product.image }
                      smell = {product.smellls}
                      size = {product.size}
                      children ={product.children}
                      product = {product.children.length > 0 ? product.children.slice().filter(a => a.id ==product.childrenItems.id ) :product}
                    >
                    </Product>
                  )
                }
            </div>
          }
          </section>
          {
            last_page > 1 && <Pagination pages={last_page} paginateProducts={paginateProducts}></Pagination>
          }
          
          </>
        }
        </>
      }
     
    </div>
  );
}


const mapStateToProps = state => ({
  productsState: state.products.items,
  last_page : state.products.last_page,
  loading: state.products.loading,

})

export default connect(
  mapStateToProps,
  fetchProducts(1),
)(Products)