import { useTranslation } from 'react-i18next';
import { useOutsideClick } from '@chakra-ui/react'
import React, { useEffect, useState } from 'react';
import axios from "axios";
import { connect } from 'react-redux';
import { filterProducts,fetchProducts,showLoader,hideLoader } from '../../actions';
import Loading from '../Cards/Loading/Loading';
function Filter({hideLoader,showLoader,showFilters,hideFilterLayout,filterProducts,fetchProducts,filters,productsState}) {
    const ref = React.useRef();
    const { t } = useTranslation();
    useOutsideClick({
        ref: ref,
        handler: () =>  hideFilterLayout(),
    })
   
    var filterClass = 'flex z-40 flex-col md:h-3/5 bg-white shadow-lg md:w-1/4 md:ml-3 md:inline rounded-lg md:flex-none absolute fixed md:static right-0 top-0 md:static hidden';
    if(showFilters){
        filterClass ='flex z-40 flex-col md:h-3/5 bg-white shadow-lg md:w-1/4 md:ml-3 md:inline rounded-lg md:flex-none absolute fixed md:static right-0 top-0 md:static';
    }
    
    const handleFilter = (event) =>{
        showLoader()
        event.preventDefault();
        const smell = event.target.elements.smell.value;
        const size = event.target.elements.size.value;
        const category = event.target.elements.category.value;
        const $filters = {'smell':smell,"size":size,'category':category};
        filterProducts($filters);

        setTimeout(() => { 
            hideLoader();
          }, 2000);
    }
    
  return (
    <div ref={ref} id='filter_md' className={filterClass}>
            {
                !filters 
                ?
                    <Loading></Loading>
                :
                    <div>
                        <form id='products_form' onSubmit={(event)=>handleFilter(event)} >
                            <div className="flex flex-col top-0 right-0 p-5 space-y-9">
                                    <div className="space-y-2">
                                        <div className="text-lg text-center p-1 border-2 border-appBasicColor rounded-full">{t('category')}</div>
                                        <div className="space-x-1 text-right items-center rtl-grid container  grid grid-cols-3 md:grid-cols-3 gap-3">
                                        <input  name='category' hidden  value='' type="radio"/>
                                            {
                                                filters.categories.map((item,key)=>
                                                <div className='m-2' key={key}>
                                                    <input name='category' value={item.id} className="w-4 h-4 m-2 accent-pink-600" type="radio"/>
                                                    <div  className="text-sm ">{item.name}</div>
                                                </div>
                                                )
                                            }
                                        
                                        </div>
                                        <div className="text-lg text-center p-1 border-2 border-appBasicColor rounded-full">{t('smell')}</div>
                                        <div className=" space-x-1 text-right items-center rtl-grid container  grid grid-cols-3 md:grid-cols-3 gap-3 ">
                                            <input  name='smell' hidden  value='' type="radio"/>
                                            {
                                                filters.smells.map((item,key)=>
                                                <div className='m-2' key={key}>
                                                    
                                                    <input name='smell'  value={item.id} className="w-4 h-4 m-2 accent-pink-600" type="radio"/>
                                                    <div  className="text-sm ">{item.name}</div>
                                                </div>
                                                )
                                            }
                                        
                                        </div>
                                        <div className="text-lg text-center p-1 border-2 border-appBasicColor rounded-full">{t('size')}</div>
                                        <div className="space-x-1 text-right items-center rtl-grid container grid grid-cols-3 md:grid-cols-4 gap-3">
                                            <input  name='size' hidden  value=''  type="radio"/>
                                            {
                                                filters.sizes.map((item,key)=>
                                                <div className='m-2' key={key}>
                                                   
                                                    <input name='size'  value={item.id} className="w-4 h-4 m-2 accent-pink-600" type="radio"/>
                                                    <div  className="text-sm ">{item.name}</div>
                                                </div>
                                                )
                                            }
                                        
                                        </div>
                                    </div>
                            </div>
                            <div className="flex flex-col m-2 h-8 justify-center text-white rounded-lg text-center items-center bg-appBasicColor">
                                <button className='w-full' >{t('search')}</button> 
                            </div>
                        </form>
                            <div className="flex flex-col m-2 h-8 justify-center border-appBasicColor border-2 text-appBasicColor rounded-lg text-center items-center bg-white">
                                <button onClick={() =>{fetchProducts();   showLoader() ;document.getElementById("products_form").reset();setTimeout(() => { hideLoader();}, 2000);} }  className='w-full' type='button' >{t('all products')}</button> 
                            </div>
                    
                            <div className="flex flex-row justify-between items-center ">
                                <a href="https://www.facebook.com/Loli-store-408273743358813/" target="_blanck" className=" p-1 rounded-lg">
                                    <div target="_blank"  type="button" className="btn btn-outline-primary waves-effect me-1">
                                        <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="none" stroke="#d5a5a5" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" className="feather feather-facebook h-6 w-6 md:h-8 md:w-8">
                                            <path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z">
                                            </path>
                                        </svg>
                                    </div> 
                                </a>
                                <a href="https://www.instagram.com/lolistore4/?hl=en" target="_blanck" className=" p-1 rounded-lg">
                                    <div target="_blank"  type="button" className="btn btn-outline-primary waves-effect ml-1 me-1 ">
                                        <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="none" stroke="#d5a5a5" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" className="feather feather-instagram h-6 w-6 md:h-8 md:w-8">
                                            <rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
                                            <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
                                            <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
                                        </svg>
                                    </div> 
                                </a>
                                <a href='#' className=" p-1 rounded-lg">
                                    <div target="_blank" type="button" className="btn btn-outline-primary waves-effect ">
                                        <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="none" stroke="#d5a5a5" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" className="feather feather-twitter h-6 w-6 md:h-8 md:w-8">
                                            <path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z">
                                            </path>
                                        </svg>
                                    </div> 
                                </a>
                            </div>
                        
                    </div>
            }
    </div>
  );
}
const mapStateToProps = state => ({
    productsState: state.products.items,
    last_page : state.products.last_page,
    filters : state.products.filters,
  })

export default connect(mapStateToProps,{filterProducts,fetchProducts,showLoader,hideLoader})(Filter);