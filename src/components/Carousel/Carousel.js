import { connect } from "react-redux"
const Carousel = ({blour_status,images}) =>{
    var className = '';
    const REACT_APP_BASE_URL = process.env.REACT_APP_BASE_URL;
    if(blour_status){
        className = "pointer-events-none opacity-40 carousel slide carousel-fade relative md:w-10/12 m-auto  "
    } 
    else{
        className = "carousel slide carousel-fade relative md:w-10/12 m-auto  ";
    }
    
    return(
        <>
        <div id="carouselExampleCrossfade" className={className} data-bs-ride="carousel">
            <div className="carousel-inner md:max-h-screen max-h-60 object-none   object-center relative w-full overflow-hidden rounded-lg">
            {
                images && Object.keys(images).length != 0
                ?
                        images.map((image,key)=>
                        key==0
                        ?
                        <div  className="carousel-item active float-left w-full">
                        <img
                            src={REACT_APP_BASE_URL + '/storage/' + image.url}
                            className="block w-full 	"
                            alt="Exotic Fruits"
                        />
                        </div>
                        :
                        <div  className="carousel-item float-left w-full">
                        <img
                            src={REACT_APP_BASE_URL + '/storage/' + image.url}
                            className="block w-full object-none  object-center"
                            alt="Exotic Fruits"
                        />
                        </div>
                        )
                :
                        <div className="carousel-item active float-left w-full blur-lg">
                        <img
                            src="https://www.teahub.io/photos/full/322-3220432_illustration.jpg"
                            className="block w-full object-none  object-center  blur-lg"
                            alt="Exotic Fruits"
                        />
                        </div>
                 
            }
            
            </div>
            <button
                className="carousel-control-prev absolute top-0 bottom-0 flex items-center justify-center p-0 text-center border-0 hover:outline-none hover:no-underline focus:outline-none focus:no-underline left-0"
                type="button"
                data-bs-target="#carouselExampleCrossfade"
                data-bs-slide="prev"
            >
                <span className="carousel-control-prev-icon inline-block bg-no-repeat" aria-hidden="true"></span>
                <span className="visually-hidden">Previous</span>
            </button>
            <button
                className="carousel-control-next absolute top-0 bottom-0 flex items-center justify-center p-0 text-center border-0 hover:outline-none hover:no-underline focus:outline-none focus:no-underline right-0"
                type="button"
                data-bs-target="#carouselExampleCrossfade"
                data-bs-slide="next"
            >
                <span className="carousel-control-next-icon inline-block bg-no-repeat" aria-hidden="true"></span>
                <span className="visually-hidden">Next</span>
            </button>
        </div>
        </>
    )
   
}

const mapStateToProps = state => ({
    blour_status: state.layout.blour_status,
    images: state.products.images,
  })
  
  export default connect(
    mapStateToProps,
)(Carousel)