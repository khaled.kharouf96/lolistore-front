import { dispatch } from "react";
import { connect } from "react-redux";
import { filterProducts,hideLoader,showLoader } from "../../actions";
function Search({hideLoader,showLoader,showFilters,hideFilterLayout,filterProducts,fetchProducts,filters,productsState}) {
    const handleFilter = (event) =>{
        showLoader()
        event.preventDefault();
        const keyword = event.target.elements.keyword.value;;
        const $filters = {'keyword':keyword};
        filterProducts($filters);
        setTimeout(() => { 
           hideLoader();
          }, 2000);
    }
  return (
    <div id='search'>
        <form onSubmit={(event)=>handleFilter(event)}>
        <section className="blur-after-click-filter flex border bg-white rounded-lg shadow-lg h-12 items-center px-2">
            
                <button type="submit" className="w-10">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width=" " stroke="currentColor" className="w-4 h-4">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z" />
                    </svg>          
                </button>
                <div className="w-full border border-none">
                    <input name="keyword" className="border border-none w-full text-right" type="text" placeholder="البحث عن منتج"/>
                </div>
            
        </section>
        </form>
    </div>
  );
}
const mapStateToProps = state => ({

  })
export default connect(mapStateToProps,{filterProducts,showLoader,hideLoader})(Search);