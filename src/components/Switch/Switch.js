import { useState } from "react";
import { connect } from "react-redux";
function Switch({handleProductLayout,handleFilterLayout,products_total_number}) {
    return (
        <div id='switch'>
            <section className="blur-after-click-filter flex  justify-between ">
                <div className="flex flex-row space-x-0">
                    <div onClick={event => handleProductLayout(true,false)} className="border  hover:bg-white border-appBasicColor rounded-l-lg p-1 shadow-sm">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="#d5a5a5" className="w-6 h-6">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M10.5 6h9.75M10.5 6a1.5 1.5 0 11-3 0m3 0a1.5 1.5 0 10-3 0M3.75 6H7.5m3 12h9.75m-9.75 0a1.5 1.5 0 01-3 0m3 0a1.5 1.5 0 00-3 0m-3.75 0H7.5m9-6h3.75m-3.75 0a1.5 1.5 0 01-3 0m3 0a1.5 1.5 0 00-3 0m-9.75 0h9.75" />
                        </svg>
                    </div>  
                    <div onClick={event => handleProductLayout(false,true)} className="border hover:bg-white border-appBasicColor rounded-r-lg p-1 shadow-sm">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="#d5a5a5" className="w-6 h-6">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M6 13.5V3.75m0 9.75a1.5 1.5 0 010 3m0-3a1.5 1.5 0 000 3m0 3.75V16.5m12-3V3.75m0 9.75a1.5 1.5 0 010 3m0-3a1.5 1.5 0 000 3m0 3.75V16.5m-6-9V3.75m0 3.75a1.5 1.5 0 010 3m0-3a1.5 1.5 0 000 3m0 9.75V10.5" />
                        </svg>
                    </div>            
                </div>
                <button onClick={event => handleFilterLayout()}  className="md:hidden" id="hamburger-button">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="#d5a5a5" className="w-8 h-8  ">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
                    </svg>
                </button>  
                <div className="text-xl hidden md:flex">
                    عدد المنتجات {products_total_number}
                </div>
            </section>
        </div>
    );
}

const mapStateToProps = state => ({
    productsState: state.products.items,
    last_page : state.products.last_page,
    filters : state.products.filters,
    products_total_number:state.products.products_total_number,
  })

export default connect(mapStateToProps)(Switch);