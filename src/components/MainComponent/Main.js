
import Switch from '../Switch/Switch';
import Search from '../Search/Search';
import Products from '../Products/Products';
import Filter from '../Filter/Filter';
import ProductDetails from '../ModalComponents/ProductDetailsComponent'
import { useState } from 'react';
function Main(props) {
  
  const [ProductLayoutState,setProductLayoutState] = useState({
    layout: {
      showAsRow : false,
      showAsCol : false,
    }
    
  })
  const [showFilters,setShowFilters] = useState(false)
  const handleProductLayout = (rowStatus,colStatus) => {
    setProductLayoutState({
      layout: 
        {
          showAsRow : rowStatus,
          showAsCol : colStatus
        },
    });
  };
  const handleFilterLayout = () => {
    setShowFilters(true);
  };
  const hideFilterLayout = () => {
    setShowFilters(false);
  };
  const styleClass = 'bg-appBackground md:flex md:flex-row justify-between' + props.blurEllementsClass
  return (
    <div id='main'>
        <section className={styleClass}>
            <div className="flex flex-col space-y-5 md:w-4/5">
                <Switch handleFilterLayout={handleFilterLayout} handleProductLayout={handleProductLayout}></Switch>
                <Search></Search>
                <Products showAsRow ={ProductLayoutState.layout.showAsRow} ></Products>
            </div>
            <Filter hideFilterLayout={hideFilterLayout} showFilters={showFilters}></Filter>
        </section>
    </div>
  );
}

export default Main;