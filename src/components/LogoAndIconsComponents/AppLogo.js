import Logo from '../../assets/img/logo.jpg'
function logo() {
    return (
        <div id='logo'>
            <div >
                <img className="h-12 w-12  rounded-lg" src={Logo} alt=""/>
            </div>
        </div>
    );
}
export default logo;