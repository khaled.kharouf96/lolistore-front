import CartIcon from './LogoAndIconsComponents/CartIcon';
import Logo from './LogoAndIconsComponents/AppLogo';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { t } from 'i18next';
import { connect } from 'react-redux';
import { useLocation } from 'react-router-dom'
import { blourLayout } from '../actions/layoutActions';
const Navbar = ({toggleShowCart,cart,blourLayout}) => {
  const { t } = useTranslation();
  const location = useLocation();
  return (
    <div id='navbar'>
        <nav className="blur-after-click-filter md:w-4/5 md:m-auto rounded-full border bg-white rounded-lg shadow-lg">
                <div className=" flex items-center  justify-between p-2 m-auto m-2">
                    <Logo></Logo>
                    {
                      
                      location.pathname != '/checkout'
                      ?
                      <>
                        <button onClick={event => toggleShowCart()} 
                        className="flex-col space-y-0 hidden md:flex"
                        >
                            <div className="flex flex-row w-2/4  rounded-full justify-center text-sm text-white bg-appBasicColor text-center">{Object. keys(cart).length}</div>
                            <CartIcon></CartIcon>
                        </button>
                        <Link className="md:hidden flex flex-col space-y-0" to="/checkout"><CartIcon></CartIcon></Link>
                      </>
                      : 
                      <Link onClick={()=>blourLayout(false)} className="hover:text-appBasicColor" to="/">{t('main')}</Link>
                    }
                    
                </div>
        </nav>
    </div>
  );
}

const mapStateToProps = state => ({
  cart: state.cart.cart,
})
export default connect(mapStateToProps,{blourLayout})(Navbar);