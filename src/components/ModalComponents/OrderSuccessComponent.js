import { resetOrder } from "../../actions/orderAction";
import { resetCart } from "../../actions/cartActions";
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
const OrderSuccessModal = ({name,phone,total,country,city,area,resetOrder,resetCart}) =>{
    return (
    <>
    <div className="ustify-center  items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
        <div className="relative w-80 my-6 mx-auto min-h-50 ">
           
            <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                <Link to="/"  onClick={()=>{resetOrder();resetCart()}} type="button" className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-800 dark:hover:text-white" data-modal-toggle="crypto-modal">
                    <svg aria-hidden="true" className="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>  
                    <span className="sr-only">Close modal</span>
                </Link>
                <div className="px-6 py-4 border-b rounded-t dark:border-gray-600">
                    <h3 className="text-center font-semibold text-gray-900 lg:text-xl dark:text-white">
                        تم الطلب بنجاح
                    </h3>
                </div>
                
                <div className="p-6 text-right">
                    <p className="text-sm font-normal text-gray-500 dark:text-gray-400">الاسم: {name}</p>
                    <p className="text-sm font-normal text-gray-500 dark:text-gray-400">سعر الطلب : {total} ليرة سورية </p>
                    <p className="text-sm font-normal text-gray-500 dark:text-gray-400">الموبايل : {phone}</p>
                    <p className="text-sm font-normal text-gray-500 dark:text-gray-400">المدينة : {city}</p>
                    <p className="text-sm font-normal text-gray-500 dark:text-gray-400">المنطقة : {area}</p>
                </div>
            </div>
        </div>
        
    </div>
    <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
    </>
  );
}
const mapStateToProps = state => ({
    shipping_cost : state.cart.shipping_cost,
    cart : state.cart.cart,

  })
  
  export default connect(mapStateToProps,{resetOrder,resetCart})(OrderSuccessModal);