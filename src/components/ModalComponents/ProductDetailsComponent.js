import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import NoOptions from '../Cards/No-Options/NoOptions';
import { connect } from "react-redux";
import ReactHtmlParser from 'react-html-parser'; 
import { addToCart } from '../../actions/cartActions';
import {notify} from 'react-notify-toast';

function ProductDetails({setShowProductDetails,id,name,price,description,image,children,cart,addToCart,product}) {
    const { t } = useTranslation();
    const [productItems , setProduct] = useState(Array.isArray(product) ? Object.assign({}, product)['0'] : product)
    const changePriceAndImage = ($product) => {setProduct($product)}
    const [test,setTest] = useState(productItems.image)
    return (
    <>
    <div
    className="justify-center  items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
    >
            <div className="relative w-auto my-6 mx-auto min-h-50 max-w-3xl">
                {/* <!-- Modal content --> */}
                <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                    {/* <!-- Modal header --> */}
                    <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                        <h3 className="text-xl font-semibold text-gray-900 dark:text-white">
                            {name}
                        </h3>
                        <button onClick={ () => setShowProductDetails(false)}  className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white" data-modal-toggle="defaultModal">
                            <svg aria-hidden="true" className="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                            <span className="sr-only">Close modal</span>
                        </button>
                    </div>
                    {/* <!-- Modal body --> */}
                    <div className="flex flex-row space-x-3 p-3">
                        <div className="flex flex-col space-y-2 w-2/4">
                            <div className="flex justify-center items-center">
                                <img className="w-1/2 p-2 hover:p-0 max-h-56 " src={productItems.image} alt=""/>
                            </div>
                            <div className="text-lg text-right pr-4 text-amber-300 ">
                                {productItems.price}.SP
                            </div>
                            <div className="text-xs h-56 overflow-auto md:h-auto md:w-auto w-auto text-right pr-4 text-yellow-700">
                                { ReactHtmlParser (productItems.description)}
                            </div> 

                            <button onClick={() => {addToCart(productItems);notify.show('تم إضافة المنتج بنجاح!','success',2000,'green')}}   className=" flex flex-row items-center justify-center m-auto text-center text-sm md:text-md text-white px-2 py-1 rounded-lg bg-appBasicColor hover:text-appBasicColor hover:border-2 hover:border-appBasicColor hover:bg-white">
                            {t('add to cart')}
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" className="w-6 h-6 ml-2">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 6v12m6-6H6" />
                                </svg>                          
                            </button>
                        </div>
                        
                        
                        <div className="flex flex-col space-y-2 w-2/4 border-l-2">
                        {
                                children.length > 0 
                                ?
                                <div className="grid max-h-96 overflow-y-auto grid-cols-2 md:grid-cols-3 gap-4 m-2" >
                                    {
                                        children.map((product,key)=>
                                        <button onClick={event => changePriceAndImage(product)} autofocus className='flex flex-col items-center justify-center focus:border-4 focus:rounded-lg focus:border-appBasicColor focus:py-2  md:focus:p-2 '>
                                            <img   key={key} className="p-1  md:max-h-40 max-h-24   " src={product.image} alt=""/>
                                            {
                                                product.size != null ? <div className='text-center text-xs'> {product.size.name}</div> : <div></div>
                                            }
                                            {
                                                product.smell != null ? <div className='text-center text-xs'> {product.smell.name}</div> : <div></div>
                                            }
                                        </button>
                                        )
                                    }
                                 </div>
                                :
                                <NoOptions text={t('no options')}></NoOptions>
                        }
                        </div>
                    </div>
                    
                </div>
            </div>
    </div>
    <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
    </>
  );
}
const mapStateToProps = state => ({
    cart: state.cart.cart,
  })
export default connect(mapStateToProps,{addToCart})(ProductDetails);