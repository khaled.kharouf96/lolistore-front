import { TagLabel } from "@chakra-ui/react";
import { Input } from "postcss";
import { connect } from "react-redux";
import { fetchProducts,showLoader,hideLoader,setCurrentPage} from "../../actions";

function Pagination({pages,fetchProducts,showLoader,hideLoader,setCurrentPage,current_page,last_page}) {

  return (
    <div id='pagination'>
        <section className="blur-after-click-filter flex justify-center items-center space-x-2 p-2 mt-2 ">
            {
                current_page == 1 
                ?
                <button disabled  className="rounded-full  bg-paginationBackgroumd  p-1">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" className="w-4 h-4">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 19.5L8.25 12l7.5-7.5" />
                </svg>     
                </button> 
                :
                <button onClick={() =>{fetchProducts(last_page-1);   showLoader() ;setTimeout(() => { hideLoader();}, 2000);setCurrentPage(last_page-1)} }   className="rounded-full  bg-paginationBackgroumd hover:bg-appBasicColor text-paginationText hover:text-white p-1">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" className="w-4 h-4">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M15.75 19.5L8.25 12l7.5-7.5" />
                </svg>     
                </button> 
            }
            
            <div className="space-x-1  flex">
                {
                    pages > 1
                    ?
                    Array.apply(0, Array(pages)).map((x, key)  =>
                        
                                <div value={key} className="mr-1">
                                    <input name="filter-inpute" type="radio" className="hidden" id={key+1} />
                                        {
                                            key+1 == current_page

                                            ?
                                                <label  for={key+1}  onClick={() =>{;fetchProducts(key+1);   showLoader() ;setTimeout(() => { hideLoader();}, 2000);setCurrentPage(key+1)} }  className="rounded-full md:text-sm text-xs  focus:text-white active:text-white text-white bg-appBasicColor md:p-2  p-1">
                                                {key+1}                        
                                                </label>
                                            :
                                                <label  for={key+1}  onClick={() =>{;fetchProducts(key+1);   showLoader() ;setTimeout(() => { hideLoader();}, 2000);setCurrentPage(key+1)} }  className="rounded-full md:text-sm text-xs bg-paginationBackgroumd focus:text-white active:text-white hover:text-white hover:bg-appBasicColor text-paginationText md:p-2 p-1">
                                                {key+1}                        
                                                </label>
                                        }
                                </div>
                    )
                    
                    :
                    <div></div>
                    
                }
            </div>
            {
                current_page == last_page 
                ?
                <button disabled className="rounded-full bg-paginationBackgroumd text-paginationText p-1">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" className="w-4 h-4">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                    </svg>                           
                </button> 
                :
                <button onClick={() =>{fetchProducts(current_page+1);   showLoader() ;setTimeout(() => { hideLoader();}, 2000);setCurrentPage(current_page+1)} } className="rounded-full bg-paginationBackgroumd hover:text-white hover:bg-appBasicColor text-paginationText p-1">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" className="w-4 h-4">
                        <path stroke-linecap="round" stroke-linejoin="round" d="M8.25 4.5l7.5 7.5-7.5 7.5" />
                    </svg>                           
                </button> 
            }
          
        </section>
    </div>
  );
}

const mapStateToProps = state => ({
    current_page : state.products.current_page,
    last_page :state.products.last_page,
  })

export default connect(mapStateToProps,{fetchProducts,showLoader,hideLoader,setCurrentPage})(Pagination);