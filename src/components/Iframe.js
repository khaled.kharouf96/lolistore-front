import { useState } from "react";
import Iframeicon from "./LogoAndIconsComponents/IframeIcon";
import { useOutsideClick } from '@chakra-ui/react'
import React from 'react';
import ReactDOM from 'react-dom';
function  Iframe({toggleBlurBackground}) {

  const ref = React.useRef();
  // React.useEffect(() => {
  //     setIsModalOpen(showCart)
  // })
  useOutsideClick({
      ref: ref,
      handler: () =>  setIframState({
        status : false,
      }),
  })
  const [ IframeState,setIframState] = useState({
    status : false,
  })

  const handleShowIframe = () => {
    setIframState({
        status : !IframeState.status,
    });
    toggleBlurBackground();
  }
  return (
    <div id='iframe'>
        {
            IframeState.status
            ?
            <iframe  ref={ref} id="iframe-section" className=" shadow-lg bottom-14  md:w-2/4 w-4/5	 !bg-white z-40 h-2/4 mx-2 fixed" src="/orders" frameborder="0">
            </iframe>
            :
            <iframe hidden ref={ref} id="iframe-section" className=" shadow-lg bottom-14  md:w-2/4 w-4/5	h-3/5 bg-white z-40 h-2/4 mx-2 fixed" frameborder="0">
            </iframe>
        }
        <button onClick={event => handleShowIframe()}  className=" iframe-button fixed bottom-2 rounded-full  p-2 bg-white z-40">
            <Iframeicon doesShow={IframeState.status}></Iframeicon>
        </button> 
    </div>
  );
}

export default Iframe;