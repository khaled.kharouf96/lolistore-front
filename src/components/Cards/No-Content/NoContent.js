import { Box, Text } from '@chakra-ui/react'
import image from '../../../assets/no-content.svg'
import { useTranslation } from 'react-i18next';
function  NoContent({text,type}) {
    const { t } = useTranslation();
    return(
        <Box className="md:w-full md:h-full m-auto  w-20 h-40 flex flex-col justify-center items-center">
            <img className="w-64" src={image} alt="No Content Found" />
            <Text className="text-dark-gray py-2 text-center text-sm md:text-md">{text}</Text>
        </Box>
    )
}

export default NoContent;