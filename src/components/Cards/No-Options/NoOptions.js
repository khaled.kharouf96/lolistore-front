import { Box, Text } from '@chakra-ui/react'
function  NoOptions({text}) {

    return(
        <Box className="w-full h-[50%] flex flex-col justify-center items-center">
            <Text className="text-dark-gray py-2">{text}</Text>
        </Box>
    )
}

export default NoOptions;