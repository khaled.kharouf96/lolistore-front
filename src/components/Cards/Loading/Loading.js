import image from '../../../assets/loading.gif'
import { Box, Text } from '@chakra-ui/react'
function  Loading() {
    return(
        <Box className="w-full h-full flex flex-col justify-center items-center bg-transparent">
            <img className="w-10%" src={image}/>
        </Box>
    )
}
export default Loading;