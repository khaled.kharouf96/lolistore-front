import { useEffect, useState,useRef } from "react";
import axios from "axios";
import { useForm } from "react-hook-form";
import { setShippingCost,removeFromCart,decrementQuantity,addToCart  } from "../../../../actions/cartActions";
import { connect } from "react-redux";
import { useDispatch } from 'react-redux';
// import { dispatch } from "react";
import { useTranslation } from "react-i18next";
import { resetCart } from "../../../../actions/cartActions";
import { resetOrder } from "../../../../actions/orderAction";
import React from "react";
import {notify} from 'react-notify-toast';




const Form = ({setShippingCost,cart,shipping_cost,setShowSuccessModal,setOrderDetails,resetOrder,resetCart}) =>{
    const { register, handleSubmit, watch, formState: { errors } } = useForm();
    const dispatch = useDispatch();
    const onSubmit = data => console.log(data);
    const [selectedElements, setSelectedElements] = useState(0)
    
    // const[countries,setCountries] = useState([]);
    const[cities,setCities] = useState([]);
    const { t } = useTranslation();
    const[loader,setLoader] = useState(false);
    const ref = useRef();
    // const[cities,setCities] = useState({
    //     status : false,
    //     data : [],
    //     filteredData : []
    // });
    const[areas,setAreas] = useState({
        status : false,
        data : [],
        filteredData : [],
    });
    const  submitOrder = () =>{
        setLoader(true)
        const name= watch('name');
        const phone= watch('phone');
        const locations = { country:watch('country'),
                            city:watch('city'),
                            area:selectedElements  
                        };
        const cartItems = {
            cart: cart,
            total: cart.reduce((a,c) => a+c.price*c.count,0)+shipping_cost ?cart.reduce((a,c) => a+c.price*c.count,0): 0
        }
        let order  = {
            name : name,
            phone : phone,
            locations : locations,
            cart : cartItems
        }
        const REACT_APP_BASE_URL = process.env.REACT_APP_BASE_URL;
        const baseURL = REACT_APP_BASE_URL + `/api/v1/order/submit-order`;
        const data  =  axios.post(baseURL,order).then(res => {
            const response = res.data;
            if(response.code==200)
            {
                setLoader(false)
                setOrderDetails({
                    name: name,
                    phone: phone,
                    city:cities.find(x => x.id==locations.city).name,
                    area:areas.data.find(x => x.id==locations.area).name,
                    total:cartItems.total+shipping_cost 
                });
                setShowSuccessModal(true);
              
            }
            else{
                setLoader(false)
                notify.show('يرجى التأكد من المنطقة المختارة ','error',2000,'green')
            }
                
        });
    }
    const getLocations = async() => {
        const REACT_APP_BASE_URL = process.env.REACT_APP_BASE_URL;
        const baseURL = REACT_APP_BASE_URL + `/api/v1/location/index`;
        const data  = await axios.get(baseURL);
        const response = data.data;
        setCities(response.cities)
        // setCities({
        //     status : false,
        //     data : response.cities,
        //     filteredData : []
        // });
        setAreas({
            status : false,
            data : response.areas,
            filteredData : []
        });
    }
    // const handleCities = (country_id) =>{
    //     console.log(country_id)
    //     setCities({
    //         status : true,
    //         filteredData :  cities.data.filter(c => c.countryId === country_id),
    //         data :  cities.data
    //     });
    //     setAreas({
    //         status : false,
    //         filteredData : [],
    //         data :  areas.data
    //     });
    //     dispatch(setShippingCost(0));
    // }
      const options = () => {
        return(
            <>
                <option value={0} selected>اختر المنطقة</option>
                {areas.filteredData.map((area,key) =>
                        <option key={key} value={area.id}>{area.name}</option>
                )}
            </>                
        )
    }
    const handleAreas = (city_id) =>{
        console.log(selectedElements)
        setSelectedElements(0)
        setAreas({
            status : true,
            filteredData :  areas.data.filter(a => a.cityId === city_id),
            data :  areas.data
        });
        dispatch(setShippingCost(0));
    }
    const handleShippingCost = (area) =>{
        const item = areas.data.find(a => a.id === area)
        setSelectedElements(area)
        dispatch(setShippingCost(item.shippingCost));
    }
  
    useEffect(() => {
        getLocations()
      },[]);
    return (
    <div className="flex bg-white flex-col text-right justify-end rounded-lg shadow-lg p-3  space-y-5 ">
        {/* <!-- Head Info --> */}
        <div className="flex flex-col space-y-2">
            <div className="text-slate-500 text-lg font-bold">البيانات المطلوبة</div>
            <div className="text-slate-400 text-sm">يرجى العلم ان كلفة الشحن تختلف من منطقة الى
                اخرى
            </div>
        </div>

        {/* <!-- Form --> */}
        <form onSubmit={handleSubmit(submitOrder)}>
            <div className="flex flex-col space-y-2">
                <div className="flex flex-col">
                    <label for="name" >:الاسم</label>
                    <input  name="name"  aria-invalid={errors.name ? "true" : "false"}  {...register("name", { required: true })} className="text-right border-2 rounded-lg p-1 px-2 mt-2" id="name" placeholder="الاسم الكامل" type="text"/>
                    {errors.name?.type === 'required' && <p classNsame="text-xl" role="alert">{t('the name is required')}</p>}
                </div>
                <div className="flex flex-col">
                    <label for="phone">:رقم الموبايل</label>
                    <input aria-invalid={errors.phone ? "true" : "false"} name="phone"  {...register("phone", { required: true,minLength: 10 ,pattern: /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/})} className="text-right border-2 rounded-lg p-1 mt-2 px-2" id="phone" type="text" placeholder="رقم الموبايل (********09 )"/>
                    {console.log(errors.phone)}
                    {errors.phone?.type === 'required' && <p classNsame="text-xl" role="alert">{t('the phone is required')}</p>}
                    {errors.phone?.type == 'minLength' && <p classNsame="text-xl" role="alert">{t('the phone must be Consist of 10 characters')}</p>}
                </div>
                {/* <div>
                    <label for="countries" className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">:البلد</label>
                    <select  name="country"  placeholder="اختر البلد" aria-invalid={errors.country ? "true" : "false"} {...register("country", { required: true,min: 1})}  onChange={(event)=>handleCities(event.target.value)}  id="countries" className="text-right block p-2 mb-2 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        <option selected value={0}>اختر البلد</option>
                        {
                            countries.map(country =>
                                <option  value={country.id}>{country.name}</option>
                            )
                        }
                    </select>
                    {errors.country?.type === 'min' && <p classNsame="text-xl" role="alert">البلد مطلوب</p>}
                    {errors.country?.type === 'required' && <p classNsame="text-xl" role="alert">البلد مطلوب</p>}
                </div> */}
                <div>
                    <label for="cites" className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">:المدينة</label>
                    {
                        <select  name="city" aria-invalid={errors.city ? "true" : "false"} {...register("city", { required: true,min: 1})}   onChange={(event)=>handleAreas(event.target.value)} id="cites" className="text-right block p-2 mb-2 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            <option id='option'  value={0} selected>اختر المدينة</option>
                            {
                                cities.map((city,key) =>
                                        
                                        <option key={key} value={city.id}>{city.name}</option>
                                )
                            }
                        </select>
                    }
                    {errors.city?.type === 'min' && <p classNsame="text-xl" role="alert">المدينة مطلوبة</p>}
                    {errors.city?.type === 'required' && <p classNsame="text-xl" role="alert">المدينة مطلوبة</p>}
                </div>
                <div>
                    <label for="areas" className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">:المنطقة</label>
                    {
                        areas.status
                        ?
                        <>
                        <select  name="area" aria-invalid={errors.area ? "true" : "false"} 
                                {...register("area", { required: true,min: 1})}  
                                onChange={(event)=>handleShippingCost(event.target.value)} 
                                value={selectedElements}
                                id="areas"
                                 className="text-right block p-2 mb-2 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            {options()}
                        </select>
                        {errors.area?.type === 'min' && <p classNsame="text-xl" role="alert">المنطقة مطلوبة</p>}
                        </>
                        :
                        <select  disabled className="text-right block p-2 mb-2 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        <option selected >اختر المنطقة</option>
                        </select>

                    }
                </div>
            </div>

            {/* <!-- Submit and Total --> */}
            <div className="flex flex-row justify-between items-center">
                <div className="bg-amber-300 text-white font-bold p-2">{shipping_cost ?cart.reduce((a,c) => a+c.price*c.count,0)+shipping_cost: cart.reduce((a,c) => a+c.price*c.count,0)} S.P</div>
                {
                    Object. keys(cart).length>0
                    ?
                    <button type="submit" className="p-2 text-white bg-appBasicColor rounded-lg">
                        {
                            loader
                            ?
                                <div class="flex justify-center items-center">
                                    <div class="spinner-border animate-spin inline-block w-8 h-8 border-4 rounded-full" role="status">
                                        <span class="visually-hidden">Loading...</span>
                                    </div>
                                </div>
                            :

                                 t('send_order')

                        }
                    </button>
                    :
                    <button disabled  type="submit" className="disabled:opacity-50 p-2 text-white bg-appBasicColor rounded-lg">
                    {t('send_order')}
                    </button>
                }
            </div>
        </form>
    </div>
    )
}
const mapStateToProps = state => ({
    shipping_cost : state.cart.shipping_cost,
    cart : state.cart.cart,

  })
  
  export default connect(mapStateToProps,{removeFromCart,addToCart,decrementQuantity,setShippingCost,resetOrder,resetCart})(Form);