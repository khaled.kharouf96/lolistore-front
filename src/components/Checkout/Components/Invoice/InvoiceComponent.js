import { useEffect, useState } from "react";
import { connect } from "react-redux";
import { removeFromCart,decrementQuantity,addToCart } from "../../../../actions/cartActions";
const Invoice = ({cart,removeFromCart,addToCart,decrementQuantity,shipping_cost}) =>{
    const total_price = cart.reduce((a,c) => a+c.price*c.count,0);
    return(
        <div className="h-2/4 md:w-2/4 flex  flex-col space-y-3 rounded-lg shadow-lg p-8 space-y-4 bg-white product-hover-shadow">
            <div className="flex flex-row-reverse text-lg font-bold">تفاصيل الفاتورة</div>
            <div className="flex flex-col space-y-2">
                <div className="flex flex-row-reverse justify-between items-center">
                    <div>سعر المنتجات</div>
                    <div>{cart.reduce((a,c) => a+c.price*c.count,0)} S.P</div>
                </div>
                <div className="flex flex-row-reverse justify-between items-center border-b-2 pb-1 ">
                    <div>كلفة الشحن</div>
                    <div>{shipping_cost ? shipping_cost:0} S.P</div>
                </div>

                <div className="flex flex-row-reverse justify-between items-center">
                    <div className="text-lg font-bold">التكلفة الإجمالية</div>
                    <div className="bg-amber-300 text-white font-bold p-1">{shipping_cost ? total_price + shipping_cost: total_price} S.P</div>
                </div>
            </div>
        </div>
    );
}

const mapStateToProps = state => ({
    cart: state.cart.cart,
    shipping_cost:state.cart.shipping_cost,
  })
export default connect(mapStateToProps,{removeFromCart,addToCart,decrementQuantity})(Invoice);