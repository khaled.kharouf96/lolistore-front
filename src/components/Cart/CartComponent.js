
import { useOutsideClick } from '@chakra-ui/react'
import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import NoContent from '../Cards/No-Content/NoContent';
import NoOptions from '../Cards/No-Options/NoOptions';
import { useTranslation } from 'react-i18next';
import {notify} from 'react-notify-toast';
import { removeFromCart,decrementQuantity,addToCart } from '../../actions/cartActions';
function Cart({showCart,hideShowCart,cart,removeFromCart,decrementQuantity,addToCart}) {
    const ref = React.useRef();
    const { t } = useTranslation();
    // React.useEffect(() => {
    //     setIsModalOpen(showCart)
    // })
    useOutsideClick({
        ref: ref,
        handler: () =>  hideShowCart(),
    })
   
    // }
    const style = {
        marginLeft: '64.1vmax',
        marginTop: '3vmax'
    }

  return (
    <div  id='cart' style={style}  className="flex flex-col rounded-lg shadow-lg absolute fixed w-1/4 top-16 mt-2  bg-white shadow-lg  md:flex  ">
    {
        showCart 
        ?
        <div ref={ref} className="flex flex-col space-y-0  ">
            <div className="flex flex-row justify-between items-center border-b-2  border-gray p-3">
                <div className="text-sm rounded-lg border-2 border-appBasicColor bg-appBasicColor text-white p-1">{cart ? Object. keys(cart).length : 0} {t('items')}</div>
                <div className="text-lg ">{t('my cart')}</div>
            </div>
            <div className="h-64 flex flex-col overflow-auto overflow-x-hidden">
                {
                    !cart || Object. keys(cart).length == 0
                    ? 
                        <NoOptions text={t('cart empty')}></NoOptions>
                    :
                    cart.map((product,key) => 
                        <div key={key} className="product-cart flex flex-row-reverse justify-between  border-b-2  border-gray p-3 hover:bg-appBackground">
                        {/* <!-- Image --> */}

                        <div className="flex flex-row items-center mx-3  ">
                            <img className="text-sm w-10 h-10" src={product.image} alt=""/>
                        </div>

                        <div className="flex flex-row items-center ">
                            <div className="text-sm">{product.name}</div>
                        </div>
                      
        
                        {/* <!-- Counter --> */}
                        <div className="flex flex-row justify-center items-center ">
                            <button onClick={()=>addToCart(product)} className="rounded-lg text-white bg-appBasicColor -mr-2 px-1 py-1 z-40"> 
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" className="w-4 h-4">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M12 6v12m6-6H6" />
                                </svg>
                            </button>
                            <input readOnly className="w-2/5 border-2 rounded-lg text-center" value={product.count}  type="number"/>
                            {
                                product.count > 1
                                ?
                                <button onClick={()=>decrementQuantity(product)} className="rounded-lg text-white bg-appBasicColor -ml-2 px-1 py-1"> 
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" className="w-4 h-4">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M18 12H6" />
                                    </svg>
                                </button>
                                :
                                <button disabled className="rounded-lg text-white bg-appBasicColor -ml-2 px-1 py-1"> 
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" className="w-4 h-4">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M18 12H6" />
                                    </svg>
                                </button>
                            }
                            
                        </div>
        
                        {/* <!-- Price --> */}
                        <div className="text-xs items-center flex ml-8">{product.count * product.price} SP</div>
    
                        {/* <!-- Remove Product --> */}
                        <div className="remove-from-cart  left-0 " >
                            <button onClick={() => {removeFromCart(product);notify.show('تم إزالة المنتج بنجاح!','success',1000,'green')}}>
                                <svg id="iframe-icon-exit" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="gray" className="w-6 h-6  hover:w-8  hover:h-8">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
                                </svg>
                            </button>
                        </div>
                        </div>
                    )
                }
            </div>

            {/* <!-- Total --> */}
            <div className="flex flex-row justify-between items-center   border-gray p-3">
                <div className="text-sm rounded-lg border-2 border-appBasicColor text-appBasicColor font-semi bold  p-1">{cart.reduce((a,c) => a+c.price*c.count,0) ?cart.reduce((a,c) => a+c.price*c.count,0): 0} S.P</div>
                <div className="text-lg ">:{t('total')}</div>
            </div>

            {/* <!-- checkout button --> */}
            <Link to="/checkout" className="flex flex-col justify-center items-center border-b-2  border-gray p-3">
                <div className="bg-appBasicColor text-center text-white rounded-lg w-11/12 p-2">عرض السلة</div>
            </Link>

        </div>
        : 
        null
    }
    </div>
  );
}
const mapStateToProps = state => ({
    cart: state.cart.cart,
  })
export default connect(mapStateToProps,{removeFromCart,decrementQuantity,addToCart})(Cart);