
import AppRoutes from './Routes/AppRoutes';
import { BrowserRouter ,Routes,Route,Redirect} from 'react-router-dom';
import Home from './pages/Home/Home';
import Checkout from './pages/Checkout/Checkout';
import Orders from './pages/Orders/Orders';

function App() {
  return (
      <BrowserRouter>
        <Routes>
              <Route
                path="checkout"
                element={
                    <Checkout />
                  }
              />
              <Route
                index
                path="/"
                element={
                    <Home />
                  }
              />
               <Route
                index
                path="orders"
                element={
                    <Orders />
                  }
              />
        </Routes>
      </BrowserRouter>
  );
}

export default App;
