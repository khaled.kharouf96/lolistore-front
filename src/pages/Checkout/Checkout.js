import Navbar from "../../components/Navbar";
import { removeFromCart,decrementQuantity,addToCart,setShippingCost } from "../../actions/cartActions";
import ReactHtmlParser from 'react-html-parser'
import { connect } from "react-redux";
import NoOptions from "../../components/Cards/No-Options/NoOptions";
import { useTranslation } from 'react-i18next';
import { useEffect, useState } from "react";
import Invoice from "../../components/Checkout/Components/Invoice/InvoiceComponent";
import Form from "../../components/Checkout/Components/Form/Form";
import OrderSuccessModal from "../../components/ModalComponents/OrderSuccessComponent";
const Checkout = ({cart,removeFromCart,addToCart,decrementQuantity,setShippingCost}) =>{
    const { t } = useTranslation();
    const[showSuccessModal,setShowSuccessModal] = useState(false)
    const[orderDetails,setOrderDetails] = useState({
        name: '',
        total: 0,
        country: '',
        city: '',
        area:'',
    })
    useEffect(
        () => setShippingCost(0),[]
    )
    return (
        <div className="bg-appBackground space-y-4 p-5">
        {/* <!-- Navbar --> */}
       <Navbar></Navbar>
  
        {/* <!-- Main Section --> */}
        <section className="flex flex-col space-y-6 ">
  
          <div className="md:flex md:flex-row-reverse justify-between md:space-y-0  space-y-3">
                {
                    !cart || Object. keys(cart).length == 0
                    ? 
                        <NoOptions text={t('cart empty')}></NoOptions>
                    :
                    <>
                        {/* <!-- Products for sm screen --> */}
                        <div className="flex flex-col space-y-6 md:hidden">
                
                            {
                                cart.map(product => 
                                    <div className="flex flex-col bg-white rounded-lg shadow-lg p-3 space-y-5 product-hover-shadow">
                                <div className="p-8 items-center flex justify-center  ">
                                    <img className="max-h-80"  src={product.image} alt=""/>
                                </div>
                                <div className="flex text-xl flex-row-reverse">
                                    {product.name}
                                </div>
                                <div className="flex flex-row-reverse space-x-1  items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#FF9F43" className="w-4 h-4">
                                        <path fill-rule="evenodd" d="M10.788 3.21c.448-1.077 1.976-1.077 2.424 0l2.082 5.007 5.404.433c1.164.093 1.636 1.545.749 2.305l-4.117 3.527 1.257 5.273c.271 1.136-.964 2.033-1.96 1.425L12 18.354 7.373 21.18c-.996.608-2.231-.29-1.96-1.425l1.257-5.273-4.117-3.527c-.887-.76-.415-2.212.749-2.305l5.404-.433 2.082-5.006z" clip-rule="evenodd" />
                                    </svg>
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#FF9F43" className="w-4 h-4">
                                        <path fill-rule="evenodd" d="M10.788 3.21c.448-1.077 1.976-1.077 2.424 0l2.082 5.007 5.404.433c1.164.093 1.636 1.545.749 2.305l-4.117 3.527 1.257 5.273c.271 1.136-.964 2.033-1.96 1.425L12 18.354 7.373 21.18c-.996.608-2.231-.29-1.96-1.425l1.257-5.273-4.117-3.527c-.887-.76-.415-2.212.749-2.305l5.404-.433 2.082-5.006z" clip-rule="evenodd" />
                                    </svg>
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#FF9F43" className="w-4 h-4">
                                        <path fill-rule="evenodd" d="M10.788 3.21c.448-1.077 1.976-1.077 2.424 0l2.082 5.007 5.404.433c1.164.093 1.636 1.545.749 2.305l-4.117 3.527 1.257 5.273c.271 1.136-.964 2.033-1.96 1.425L12 18.354 7.373 21.18c-.996.608-2.231-.29-1.96-1.425l1.257-5.273-4.117-3.527c-.887-.76-.415-2.212.749-2.305l5.404-.433 2.082-5.006z" clip-rule="evenodd" />
                                    </svg>
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#FF9F43" className="w-4 h-4">
                                        <path fill-rule="evenodd" d="M10.788 3.21c.448-1.077 1.976-1.077 2.424 0l2.082 5.007 5.404.433c1.164.093 1.636 1.545.749 2.305l-4.117 3.527 1.257 5.273c.271 1.136-.964 2.033-1.96 1.425L12 18.354 7.373 21.18c-.996.608-2.231-.29-1.96-1.425l1.257-5.273-4.117-3.527c-.887-.76-.415-2.212.749-2.305l5.404-.433 2.082-5.006z" clip-rule="evenodd" />
                                    </svg>
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#FF9F43" className="w-4 h-4">
                                        <path fill-rule="evenodd" d="M10.788 3.21c.448-1.077 1.976-1.077 2.424 0l2.082 5.007 5.404.433c1.164.093 1.636 1.545.749 2.305l-4.117 3.527 1.257 5.273c.271 1.136-.964 2.033-1.96 1.425L12 18.354 7.373 21.18c-.996.608-2.231-.29-1.96-1.425l1.257-5.273-4.117-3.527c-.887-.76-.415-2.212.749-2.305l5.404-.433 2.082-5.006z" clip-rule="evenodd" />
                                    </svg>
                                </div>
                    
                                <div className="flex flex-row-reverse  ">
                                <button onClick={()=>addToCart(product)} className="rounded-lg text-white bg-appBasicColor -mr-2 px-1 py-1 z-40"> 
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" className="w-4 h-4">
                                        <path stroke-linecap="round" stroke-linejoin="round" d="M12 6v12m6-6H6" />
                                    </svg>
                                </button>
                                <input readOnly className="w-2/5 border-2 rounded-lg text-center" value={product.count}  type="number"/>
                                {
                                    product.count > 1
                                    ?
                                    <button onClick={()=>decrementQuantity(product)} className="rounded-lg text-white bg-appBasicColor -ml-2 px-1 py-1"> 
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" className="w-4 h-4">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M18 12H6" />
                                        </svg>
                                    </button>
                                    :
                                    <button disabled className="rounded-lg text-white bg-appBasicColor -ml-2 px-1 py-1"> 
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" className="w-4 h-4">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M18 12H6" />
                                        </svg>
                                    </button>
                                }
                                </div>
                    
                                <div className="text-center text-lg font-bold text-purple-500	">
                                {product.count * product.price} S.P
                                </div>
                    
                                <div className="flex flex-col items-center w-full">
                                    <button onClick={()=>removeFromCart(product)} className="flex w-full  items-center justify-center space-x-1 text-lg border-1 bg-slate-50 text-black p-2 text-center hover:bg-red-300 hover:text-white rounded-br-lg">
                                        Remove
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" className="w-6 h-6">
                                            <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
                                            </svg>                                                  
                                    </button>
                                </div>
                                    </div>
                                )
                            }
                            
                
                        </div>
            
                        {/* <!-- Products for md screen --> */}
                        <div className="w-2/3 flex-col space-y-6 hidden md:flex ml-2">
                            {
                                cart.map(product =>
                                    <div className="flex  md:flex-row-reverse rounded-lg shadow-lg bg-white bg-white ">
                                        {/* <!-- Image --> */}
                                        <div className="h-48 p-3 hover:p-0 md:basis-1/4">
                                            <img className="h-full w-full" src={product.image} alt=""/>
                                        </div>
                    
                                        {/* <!-- Product Info --> */}
                                        <div className=" flex flex-col text-right p-2 mb-5  md:basis-2/4 ">
                                            <h1 className="text-xl font-semibold text-gray-600">{product.name}</h1>
                                            {/* <!-- For row grid --> */}
                                            <div className="flex hidden md:flex md:justify-end md:my-2">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#FF9F43" className="w-4 h-4">
                                                    <path fill-rule="evenodd" d="M10.788 3.21c.448-1.077 1.976-1.077 2.424 0l2.082 5.007 5.404.433c1.164.093 1.636 1.545.749 2.305l-4.117 3.527 1.257 5.273c.271 1.136-.964 2.033-1.96 1.425L12 18.354 7.373 21.18c-.996.608-2.231-.29-1.96-1.425l1.257-5.273-4.117-3.527c-.887-.76-.415-2.212.749-2.305l5.404-.433 2.082-5.006z" clip-rule="evenodd" />
                                                </svg>
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#FF9F43" className="w-4 h-4">
                                                    <path fill-rule="evenodd" d="M10.788 3.21c.448-1.077 1.976-1.077 2.424 0l2.082 5.007 5.404.433c1.164.093 1.636 1.545.749 2.305l-4.117 3.527 1.257 5.273c.271 1.136-.964 2.033-1.96 1.425L12 18.354 7.373 21.18c-.996.608-2.231-.29-1.96-1.425l1.257-5.273-4.117-3.527c-.887-.76-.415-2.212.749-2.305l5.404-.433 2.082-5.006z" clip-rule="evenodd" />
                                                </svg>
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#FF9F43" className="w-4 h-4">
                                                    <path fill-rule="evenodd" d="M10.788 3.21c.448-1.077 1.976-1.077 2.424 0l2.082 5.007 5.404.433c1.164.093 1.636 1.545.749 2.305l-4.117 3.527 1.257 5.273c.271 1.136-.964 2.033-1.96 1.425L12 18.354 7.373 21.18c-.996.608-2.231-.29-1.96-1.425l1.257-5.273-4.117-3.527c-.887-.76-.415-2.212.749-2.305l5.404-.433 2.082-5.006z" clip-rule="evenodd" />
                                                </svg>
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#FF9F43" className="w-4 h-4">
                                                    <path fill-rule="evenodd" d="M10.788 3.21c.448-1.077 1.976-1.077 2.424 0l2.082 5.007 5.404.433c1.164.093 1.636 1.545.749 2.305l-4.117 3.527 1.257 5.273c.271 1.136-.964 2.033-1.96 1.425L12 18.354 7.373 21.18c-.996.608-2.231-.29-1.96-1.425l1.257-5.273-4.117-3.527c-.887-.76-.415-2.212.749-2.305l5.404-.433 2.082-5.006z" clip-rule="evenodd" />
                                                </svg>
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#FF9F43" className="w-4 h-4">
                                                    <path fill-rule="evenodd" d="M10.788 3.21c.448-1.077 1.976-1.077 2.424 0l2.082 5.007 5.404.433c1.164.093 1.636 1.545.749 2.305l-4.117 3.527 1.257 5.273c.271 1.136-.964 2.033-1.96 1.425L12 18.354 7.373 21.18c-.996.608-2.231-.29-1.96-1.425l1.257-5.273-4.117-3.527c-.887-.76-.415-2.212.749-2.305l5.404-.433 2.082-5.006z" clip-rule="evenodd" />
                                                </svg>
                                            </div>
                                            {/* <div className="flex flex-col items-center">
                                                <p className="text-sm text-darkGrayishBlue">{ ReactHtmlParser (product.description)}</p>
                                            </div> */}
                                        </div>
                        
                                        {/* <!-- Remove Product and Counter --> */}
                                        <div className="h-8 flex flex-row md:flex-col md:items-center md:space-y-2  space-x-0 justify-between md:basis-1/4 pb-0 md:h-4/5">
                                            <div className=" text-lg hidden md:flex">{product.count * product.price} s.p</div>
                                            <div className=" flex justify-center items-center space-x-1  border-1  p-1 w-2/4 text-center  md:w-4/5 rounded-lg h-4/5 text-lg md:p-1 ">
                                                <button onClick={() => addToCart(product)} className="rounded-lg text-white bg-appBasicColor  px-1 py-1 "> 
                                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" className="w-4 h-4">
                                                        <path stroke-linecap="round" stroke-linejoin="round" d="M12 6v12m6-6H6" />
                                                    </svg>
                                                </button>
                                                <input disabled className="w-2/5 border-2 rounded-lg text-center " value={product.count}  type="number"/>
                                                {
                                                    product.count>1
                                                    ?
                                                    <button onClick={() => decrementQuantity(product)} className="rounded-lg text-white bg-appBasicColor px-1 py-1"> 
                                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" className="w-4 h-4">
                                                            <path stroke-linecap="round" stroke-linejoin="round" d="M18 12H6" />
                                                        </svg>
                                                    </button>
                                                    :

                                                    <button className="rounded-lg text-white bg-appBasicColor px-1 py-1"> 
                                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" className="w-4 h-4">
                                                            <path stroke-linecap="round" stroke-linejoin="round" d="M18 12H6" />
                                                        </svg>
                                                    </button>
                                                }
                                            </div>
                                            <button onClick={()=>removeFromCart(product)}   className="flex  items-center justify-center space-x-1 text-lg border-1 bg-slate-50 text-black p-1  text-center rounded-br-lg w-4/5 md:rounded-lg h-4/5  ">
                                                Remove
                                                <svg  xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="gray" className="w-6 h-6">
                                                    <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
                                                </svg>                          
                                            </button>
                                        </div>
                                    </div>
                                )
                            }
                        </div>
                    </>
                }
            <Invoice></Invoice>
            {showSuccessModal &&    <OrderSuccessModal 
                                        name={orderDetails.name}
                                        phone={orderDetails.phone}
                                        total={orderDetails.total}
                                        city={orderDetails.city}
                                        area={orderDetails.area}
                                    >
                                    </OrderSuccessModal>}
          </div>
  
  
          {/* <!-- Input Form --> */}
          <Form setOrderDetails={setOrderDetails} setShowSuccessModal={setShowSuccessModal}></Form>
        </section>
  
        </div>
    )
}
const mapStateToProps = state => ({
    cart: state.cart.cart,
  })
export default connect(mapStateToProps,{removeFromCart,addToCart,decrementQuantity,setShippingCost})(Checkout);