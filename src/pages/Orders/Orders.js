import { useTranslation } from "react-i18next";
import axios from "axios";
import { useForm } from "react-hook-form";
import { useEffect,useState} from "react";
import NoContent from "../../components/Cards/No-Content/NoContent";
const Orders = () => {
  const { t } = useTranslation();
  const { register, handleSubmit, watch, formState: { errors } } = useForm();
  const [orders,setOrders] = useState([]);
  const [loader,setLoader] = useState(false);
  const getUserOrders =  () =>{
      setLoader(true)
      setOrders([]); 
      const phone = watch('phone');
      const REACT_APP_BASE_URL = process.env.REACT_APP_BASE_URL;
      const baseURL = REACT_APP_BASE_URL + `/api/v1/order/user/all-orders`;
      const data  = axios.post(baseURL,{'phone' : phone }).then(res => {
        const response = res.data;
         setOrders(response.orders); 
         setLoader(false)
      });
      
     
  }
  return (
    <div className="overflow-x-hidden !bg-white">
      <div className="flex flex-col space-y-0  p-2 m-2">
        <div className="flex flex-col items-center space-y-3 md:space-y-0 md:items-none md:flex-row-reverse justify-between ">
          <div className="flex flex-col ">
            <div className="flex text-lg font-bold">قائمة الطلبات السابقة</div>
          <hr />
          </div>
          <div className="flex flex-row md:space-x-1 space-x-2  items-center">
            <form onSubmit={handleSubmit(getUserOrders)}>
            <input
              aria-invalid={errors.phone ? "true" : "false"}  name="phone"  {...register("phone", { required: true,minLength: 10 ,pattern: /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/})}  id="phone" type="text" placeholder="رقم الموبايل (********09 )"
              className="border-2 text-right px-2 py-1 rounded-lg "
            />
            {errors.phone?.type === 'required' && <p classNsame="text-xl" role="alert">phone is required</p>}
            {errors.phone?.type == 'minLength' && <p classNsame="text-xl" role="alert">phone must be equal 10 numbers</p>}
            <button type="submit">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke-width=" "
                stroke="currentColor"
                className="w-8 h-8 mx-2"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
                />
              </svg>
            </button>
            </form>
          </div>
        </div>
      </div>
      <div class="flex flex-col">
        <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div class="py-2 inline-block min-w-full sm:px-6 lg:px-8">
            <div class="overflow-hidden">
                {
                  loader ?
                   <div class="flex justify-center items-center">
                  <div class="spinner-border animate-spin inline-block w-8 h-8 border-4 rounded-full" role="status">
                    <span class="visually-hidden">Loading...</span>
                  </div>
                  </div>

                  :
                  <>
                  {
                    orders.length ==0 
                    ?
                     <NoContent text={'لا يوجد طلبات'}></NoContent>
  
                    :
                    <table class="min-w-full text-center">
                  
                      <thead class="border-b">
                        <tr>
                          <th
                            scope="col"
                            class="text-sm font-medium text-gray-900 px-6 py-4"
                          >
                            {t('order_date')}
                          </th>
                          <th
                            scope="col"
                            class="text-xs font-medium text-gray-900 px-6 py-4"
                          >
                            {t('order_state')}
                          </th>
  
                          <th
                            scope="col"
                            class="text-xs font-medium text-gray-900 px-6 py-4"
                          >
                            {t('total_price')}
                          </th>

                        </tr>
                      </thead>
                      
                      <tbody>
                        {
                          orders.map((order,key)=>
                        <tr class="border-b">
                          <td class="text-xs text-gray-900 font-medium px-6 py-4 whitespace-nowrap">
                          {order.date}
                            </td>
                          <td class="text-xs text-gray-900 font-medium px-6 py-4 whitespace-nowrap">
                          {order.status}
                            </td>
                          <td class="text-xs text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                          {order.total_price}
                            </td>
                        </tr>
                          )
                        }
                        
                      </tbody>
                    </table>
                  }
                  </>
                }
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Orders;
