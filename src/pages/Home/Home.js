
import '../../index.css';
import Main from '../../components/MainComponent/Main';
import Iframe from '../../components/Iframe';
import Navbar from '../../components/Navbar';
import Carousel from '../../components/Carousel/Carousel';
import Cart from '../../components/Cart/CartComponent';
import { connect } from 'react-redux';
import { blourLayout } from '../../actions/layoutActions';


// import ProductDetails from './components/ModelComponents/ProductDetailsComponent';
import { useState,useEffect,useRef  } from 'react';

const Home = ({blourLayout}) => {

  const [ ProductCartState, setProductCartState ] = useState({
      showCart : false,
      blurEllementsClass : null, 
  });

  const [ BlureState, setBlureState ] = useState({
    doesBlur : false,
    blurEllementsClass : null, 
  });
  const toggleBlurBackground = () => {
    setBlureState({
      doesBlur : !BlureState.doesBlur,
      blurEllementsClass : !BlureState.doesBlur ? 'pointer-events-none opacity-40' : null ,
    });
  }
  const toggleShowCart = () => {
    blourLayout(true)
    setProductCartState({
      showCart : !ProductCartState.showCart,
    });
     setBlureState({
      doesBlur : !BlureState.doesBlur,
      blurEllementsClass : !BlureState.doesBlur ? 'pointer-events-none opacity-40' : null ,
    });
  }

  const hideShowCart = () => {
    blourLayout(false)
    setProductCartState({
      showCart : false,
    });
     setBlureState({
      doesBlur : false,
      blurEllementsClass : null ,
    });
  }


  return (
    <>
    <div className="bg-appBackground ">
      
        <div className="bg-appBackground p-5 space-y-5">
        <Navbar toggleShowCart={toggleShowCart}></Navbar>

        <Carousel className="pointer-events-none opacity-40"></Carousel>
        <Main blurEllementsClass = {BlureState.blurEllementsClass} ></Main>
        <Iframe  toggleBlurBackground={toggleBlurBackground}></Iframe>
        </div>
      <Cart showCart = {ProductCartState.showCart} hideShowCart={hideShowCart}></Cart>
    </div>
    
    </>
  );
}

const mapStateToProps = state => ({
 
})

export default connect(
  mapStateToProps,
  {blourLayout},
)(Home)
